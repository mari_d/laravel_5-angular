angular.module('LoginController', []).service('satellizer', function () {}).controller('LoginController', ['$scope', '$auth', '$location', '$routeParams', '$localStorage', '$location',

        function($scope, $auth, $localStorage, $location) {

            $scope.authenticate = function(provider) {
                $auth.authenticate(provider).then(function(response) {
                    $localStorage.token = response.data.token;
                    $scope.getAuthenticatedUser(response.data);
                    console.log(response.data);
                });
            };

        }
    ]);