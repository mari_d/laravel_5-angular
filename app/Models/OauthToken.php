<?php

namespace Todo\Models;

use Illuminate\Database\Eloquent\Model;

class OauthToken extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'oauth_tokens';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'service_user_id',
        'username',
        'email',
        'service',
        'url',
        'authKey',
        'user_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['authKey'];

    private $service;
    private $callbackUser;


    public function findOrCreate($callbackUser, $service)
    {
        $this->service = $service;
        $this->callbackUser = $callbackUser;

        $checkOauthToken = OauthToken::where('service_user_id', $this->callbackUser['id']);
        $checkUser = $this->getUser();
        if (!$checkOauthToken->first()) {
            $oauthToken = OauthToken::create([
                'service_user_id' => $this->callbackUser['id'],
                'service' => $service,
                'username' => $this->callbackUser['name'],
                'url' => $this->callbackUser['url'],
                'user_id' => $checkUser->id,
            ]);
            $oauthToken->save();
        }
        return $checkUser;
    }

    private function getUser()
    {
        $user = User::where('email', $this->callbackUser['email']);
        if (!$user->first()) {
            $user = User::create([
                'username' => $this->callbackUser['login'],
                'email' => $this->callbackUser['email'],
                'password' => bcrypt($this->callbackUser['id'] . $this->callbackUser['email']), // is it correctly???
            ]);
            $user->save();
        }else{
            $user = User::where('email', $this->callbackUser['email'])->first();
        }
        return $user;
    }

}
