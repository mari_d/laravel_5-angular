<?php 

namespace Todo\Models;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model {

	protected $fillable = [
        'body'
    ];

}
