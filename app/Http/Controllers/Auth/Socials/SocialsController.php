<?php

namespace Todo\Http\Controllers\Auth\Socials;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Todo\Http\Requests;

use Validator;
use Socialite;
use Todo\Http\Controllers\Controller;
use Todo\Models\OauthToken;
use Todo\Models\User;
use Tymon\JWTAuth\JWTAuth;
use GuzzleHttp;
use Config;

class SocialsController extends Controller
{

    private $service;
    private $request;
    private $jwtAuth;

    function __construct(Request $request, ResponseFactory $responseFactory, JWTAuth $jwtAuth)
    {
        $this->request = $request;
        $this->res = $responseFactory;
        $this->jwtAuth = $jwtAuth;
        if (isset($request->service)) {
            $this->service = $request->service;
        }
    }

    public function github(Request $request)
    {
        $client = new GuzzleHttp\Client();
        $params = [
            'code' => $request->input('code'),
            'client_id' => $request->input('clientId'),
            'client_secret' => Config::get('app.github_secret'),
            'redirect_uri' => $request->input('redirectUri')
        ];
        // Step 1. Exchange authorization code for access token.
        $accessTokenResponse = $client->request('GET', 'https://github.com/login/oauth/access_token', [
            'query' => $params
        ]);
        $accessToken = array();
        parse_str($accessTokenResponse->getBody(), $accessToken);
        // Step 2. Retrieve profile information about the current user.
        $profileResponse = $client->request('GET', 'https://api.github.com/user', [
            'headers' => ['User-Agent' => 'Satellizer'],
            'query' => $accessToken
        ]);

        $profile = json_decode($profileResponse->getBody(), true);

        // Step 3a. If user is already signed in then link accounts.
//        if ($request->header('Authorization') && $this->jwtAuth->parseToken()->authenticate())
//        {
//            return $this->res->json(['message' => 'There is already a GitHub account that belongs to you'], 409);
//        }
        // Step 3b. Create a new user && OauthToken or return an existing one.
//        else {
            $oauthToken = new OauthToken();
            $user = $oauthToken->findOrCreate($profile, $this->service);
            $user['token'] = $this->jwtAuth->fromUser($user);
            return $this->res->json($user, Response::HTTP_OK);
//        }
    }

}
